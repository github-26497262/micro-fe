// cdn链接
module.exports = {
  // cdn：模块名称和模块作用域命名（对应window里面挂载的变量名称）
  externals: {
    vue: 'Vue',
    'vue-router': 'VueRouter',
    // 三方资源
    echarts: 'echarts',
  },
  // cdn的css链接
  css: [
    // 'https://testqiniu.shuhaisc.com/index.css'
  ],
  // cdn的js链接
  js: [
    'https://cdn.staticfile.org/vue/2.6.11/vue.min.js',
    'https://cdn.staticfile.org/vue-router/3.2.0/vue-router.min.js',
    'https://cdn.jsdelivr.net/npm/echarts@4.7.0/dist/echarts.min.js',
  ]
}
