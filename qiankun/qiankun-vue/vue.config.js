const { name } = require("./package");
const devServerPort = require("../devServerPort");
const cdn = require("../commonCdn");

console.log(process.env.NODE_ENV);
const isProduction = process.env.NODE_ENV !== "development";
// 本地环境是否需要使用cdn
const devNeedCdn = false;
// 生产模式是否要独立运行
const productIndependentOperation = false;

module.exports = {
  productionSourceMap: false,
  devServer: {
    port: devServerPort[name],
    headers: {
      "Access-Control-Allow-Origin": "*",
    },
  },
  chainWebpack: (config) => {
    if (productIndependentOperation) {
      // 移除 prefetch 插件
      config.plugins.delete("prefetch");
      // 移除 preload 插件
      config.plugins.delete("preload");

      config.plugin("html").tap((args) => {
        // 生产环境或本地需要cdn时，才注入cdn
        if (isProduction || devNeedCdn) args[0].cdn = cdn;
        return args;
      });
    }
  },
  configureWebpack: (config) => {
    config.output.library = `${name}-[name]`;
    config.output.libraryTarget = "umd";
    config.output.jsonpFunction = `webpackJsonp_${name}`;
    // 用cdn方式引入，则构建时要忽略相关资源
    if (isProduction || devNeedCdn) config.externals = cdn.externals;
    if (isProduction) {
      config.devtool = false;
    }
  },
};
