import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { registerMicroApps, start } from "qiankun";

Vue.config.productionTip = false;

registerMicroApps([
  {
    name: "vue app",
    entry: process.env.VUE_APP_ENTRY_PATH,
    container: "#vue-view",
    activeRule: "/vue",
  },
]);
console.log(process.env)

start({ sandbox: { experimentalStyleIsolation: true } });

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
