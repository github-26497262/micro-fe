const { name } = require("./package");
const devServerPort = require("../devServerPort");
const cdn = require('../commonCdn')

console.log(process.env.NODE_ENV)
const isProduction = process.env.NODE_ENV !== 'development'
// 本地环境是否需要使用cdn
const devNeedCdn = false

module.exports = {
  devServer: {
    port: devServerPort[name],
    proxy: {
      "/api": {
        target: "http://localhost:3000", //API服务器的地址
        ws: true, //代理websockets
        changeOrigin: true, // 是否跨域，虚拟的站点需要更管origin
        pathRewrite: {
          "^/api": "",
        },
      },
    },
  },
  chainWebpack: config => {
    // 移除 prefetch 插件
    // config.plugins.delete('prefetch')
    // // 移除 preload 插件
    // config.plugins.delete('preload')

    config.plugin('html').tap(args => {
      // 生产环境或本地需要cdn时，才注入cdn
      if (isProduction || devNeedCdn) args[0].cdn = cdn
      return args
    })
  },
  configureWebpack: config => {
    config.output.library = `${name}-[name]`
    config.output.libraryTarget = 'umd'
    config.output.jsonpFunction = `webpackJsonp_${name}`
    // 用cdn方式引入，则构建时要忽略相关资源
    if (isProduction || devNeedCdn) config.externals = cdn.externals
    if (isProduction) {
      config.devtool = false
    }
  }
};
