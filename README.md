# Vite学习

## 目录

*   *   [1.  快速开始](#1--快速开始)

    *   [2. base 配置打包公共路径](#2-base-配置打包公共路径)

    *   [3. alias配置图片地址别名](#3-alias配置图片地址别名)

    *   [4. 构建生产版本移除log打印](#4-构建生产版本移除log打印)

    *   [5. 打包输出文件夹配置](#5-打包输出文件夹配置)

    *   [6. 配置开发环境和生产环境变量](#6-配置开发环境和生产环境变量)

    *   [7. CDN 引入](#7-cdn-引入)

        *   [安装模块:](#安装模块)

        *   [CDN引入与自动引入插件做对比：](#cdn引入与自动引入插件做对比)

    *   [8. GZIP压缩](#8-gzip压缩)

*   [9.依赖分析](#9依赖分析)

## 1.  快速开始

安装脚手架：

```bash
yarn create vite app
cd app
yarn 
```

## 2. base 配置打包公共路径

```typescript
// vite.config.ts
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  plugins: [vue()]
})
```

为防止部署项目时发生资源路径访问错误的隐患，这里配置相对路径来避免发生。

## 3. alias配置图片地址别名

> ⚠️ts需要安装`@types/node`才能识别到`node_modules`文件夹下的模块

```bash
yarn add @types/node
```

&#x20;在`vite.config.ts` 文件中，设置别名配置：

```typescript
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  resolve: {
    alias: {
      '@': resolve(__dirname, 'src'),
      '@comp': resolve(__dirname, 'src/components')
    }
  },
  plugins: [vue()]
})
```

测试配置是否生效：

```vue
// App.vue
<script setup lang="ts">
import HelloWorld from '@comp/HelloWorld.vue'
</script>
<template>
  <img alt="Vue logo" src="./assets/logo.png" />
  <HelloWorld msg="Hello Vue 3 + TypeScript + Vite" />
</template>
```

网页组件正常加载 ，说明别名配置成功

## 4. 构建生产版本移除log打印

```typescript
// vite.config.ts
export default defineConfig({
  ...
  build: {
    minify: 'terser', // 默认为esbuild
    terserOptions: {
      compress: {
        drop_console: true, // 生产环境移除console
        drop_debugger: true // 生产环境移除debugger
      }
    }
  },
  ...
})
```

## 5. 打包输出文件夹配置

未配置输出文件夹和配置了输出文件夹对比，配置后的结构更加清晰明了

```typescript
// vite.config.ts
...
export default defineConfig({
  build: {
    ...
    rollupOptions: {
      output: {
        manualChunks(id) {
            // node_modules 下文件分包
          if (id.includes("node_modules")) {
            return id
              .toString()
              .split("node_modules/")[1]
              .split("/")[0]
              .toString();
          }
        },
        chunkFileNames: "vendors/[name]-[hash].js", // 三方库文件
        entryFileNames: "js/[name]-[hash].js", // entry js文件
        assetFileNames: "[ext]/[name]-[hash].[ext]", // css文件
      },
    }
  },
  ...
})
```

## 6. 配置开发环境和生产环境变量

在**根目录**创建两个文件`.env.development`(开发环境配置文件）和`.env.production`(生产环境配置文件）

```typescript
// .env.development
VITE_BASE_API = '/mocks/get'

```

```typescript
// .env.production
VITE_BASE_API = 'https://v1.hitokoto.cn/?c=f&encode=text'

```

在App.vue中修改请求地址：

```vue
<script setup lang="ts">
import HelloWorld from '@comp/HelloWorld.vue'
import axios from 'axios'
async function fn() {
  const { data } = await axios.get(import.meta.env.VITE_BASE_API as string)
  console.log(data)
}
fn()
</script>
```

## 7. CDN 引入

### 安装模块:

```bash
yarn add vite-plugin-cdn-import -D
```

修改`vite.config.ts` 文件

```typescript
...
// 引入CDN插件
import importToCDN, { autoComplete } from 'vite-plugin-cdn-import'
...

export default defineConfig({
  ...
  plugins: [
    ...
    importToCDN({
      modules: [
        {
          name: 'vue',
          var: 'Vue',
          path: 'https://unpkg.com/vue@next'
        },
        {
          name: 'element-plus',
          var: 'ElementPlus',
          path: `https://unpkg.com/element-plus`,
          css: 'https://unpkg.com/element-plus/dist/index.css'
        }
      ]
    })
  ],
})
```

修改`main.ts` 文件：

```typescript
import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus'

createApp(App).use(ElementPlus).mount('#app')
// createApp(App).mount('#app')
```

### CDN引入与自动引入插件做对比：

CDN是全局引入，esbuild会按需加载打包更小。

## 8. GZIP压缩

vite内置GZIP压缩，nginx配置

```纯文本
#Gzip module
    gzip  on;
    gzip_disable "MSIE [1-6]\.";
    gzip_min_length 1k;
    gzip_http_version 1.1;
    gzip_types text/css text/xml application/javascript;
```

# 9.依赖分析

```纯文本
yarn add rollup-plugin-visualizer -D
```

```纯文本
// vite.config.ts
import { defineConfig } from 'vite'
import { visualizer } from "rollup-plugin-visualizer";

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  plugins: [vue(), visualizer()]
})

```
