const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");

const app = express();

// 请求日志
app.use((req, res, next) => {
  console.log(`${Date.now()} ${req.method} ${req.url}`);
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(
  session({
    secret: "keyboard cat",
    cookie: { maxAge: 60000 },
  })
);

app.post("/login", (req, res) => {
  // const {name, age} = JSON.parse(req.body)
  console.log(req.body);

  req.session.userInfo = {
    ...req.body,
    time: new Date().getTime(),
  };
  res.send({
    msg: "登录成功",
  });
});

app.get("/userInfo", (req, res) => {
  res.send({
    ...req.session.userInfo,
  });
});

app.get("/logout", (req, res) => {
  req.session.userInfo = null;
  res.send({
    msg: "退出成功",
  });
});

app.listen(3000, () => console.log(`Example app listening on port ${3000}!`));
