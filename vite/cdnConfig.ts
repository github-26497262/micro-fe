export const cdnConfig = [
  {
    name: "vue",
    var: "Vue",
    path: "https://cdn.staticfile.org/vue/3.2.36/vue.global.prod.min.js",
  },
  {
    name: "vue-router",
    var: "VueRouter",
    path: "https://cdn.staticfile.org/vue-router/4.0.16/vue-router.global.prod.min.js",
  },
  {
    name: "qs",
    var: "Qs",
    path: "https://cdn.staticfile.org/qs/6.11.0/qs.min.js",
  },
  {
    name: "element-plus",
    var: "ElementPlus",
    path: "https://cdn.staticfile.org/element-plus/2.2.8/index.full.min.js",
    css: "https://cdn.staticfile.org/element-plus/2.2.8/index.min.css",
  }
]