import { createApp } from "vue";
import ElementPlus from 'element-plus'
// import 'element-plus/dist/index.css'  CDN引入不需要引入css

import { registerMicroApps, start } from 'qiankun';
import App from "./App.vue";
import router from "./router";

registerMicroApps([
  {
    name: "vue app",
    entry: import.meta.env.VITE_APP_ENTRY_PATH,
    container: "#vue-view",
    activeRule: "/vue",
  },
]);
console.log(import.meta.env.MODE)

start({ sandbox: { experimentalStyleIsolation: true } });

const app = createApp(App);
app.use(router).use(ElementPlus).mount("#app");
