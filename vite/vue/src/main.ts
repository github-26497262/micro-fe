import { createApp } from "vue";
import {
  renderWithQiankun,
  qiankunWindow,
  QiankunProps,
} from "vite-plugin-qiankun/dist/helper";
import App from "./App.vue";
import router from "./router";

let app:any = null
function render(props: any) {
  const { container } = props;
  app = createApp(App);
  app.use(router)
  app.mount(container ? container.querySelector("#app") : "#app");
}

// some code
renderWithQiankun({
  mount(props: any) {
    console.log("mount");
    render(props);
  },
  bootstrap() {
    console.log("bootstrap");
  },
  unmount(props: any) {
    app.unmount()
  },
  update: function (props: QiankunProps): void | Promise<void> {
    throw new Error("Function not implemented.");
  },
});

if (!qiankunWindow.__POWERED_BY_QIANKUN__) {
  render({});
}
