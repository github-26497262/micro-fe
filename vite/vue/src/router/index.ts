import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  { path: '/', component: Home },
  { path: '/about', component: () => import('../views/About.vue') },
  { path: '/detail/:code', component: () => import('../views/Detail.vue') },
]

export default createRouter({
  history: createWebHistory('/vue'),
  routes
})